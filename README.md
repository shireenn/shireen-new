1. 	Create feature branch on meta-project
2.	Create folder for the module /home/dev/module-xy
2.1.	composer init
2.2.	git init
2.3.	Create a README describing the feature
2.4.	Setup the Magento folder structure
2.5.	Create modman mapping file
3.	Update composer.json of meta-project
3.1.	Include the module
3.2.	Include local vcs repository
3.3.	Install the module using “composer update”
3.4.	Implement the module
3.5.	Test the module
3.6.	Publish the module
3.7.	Update the composer.json and reference the published module
3.8.	Create a feature instance using the CloudAPI

