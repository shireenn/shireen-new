<?php
/**
 * Class Arvato_TheChallenge_IndexController provides action to dispaly a simple string
 *
 * @category    Arvato
 * @package     Arvato_TheChallenge
 * @copyright   Copyright (c) 2015 arvato
 */
class Arvato_TheChallenge_IndexController extends Mage_Core_Controller_Front_Action

{

    /**
     * Displays any simple string.
     */
    public function indexAction()
    {
       return $this->getGitCurrentBranch();
    }

    /**
     * Get the current branch name
     */
    public function getGitCurrentBranch()
    {
 $shellOutput = [];
        exec('git branch | ' . "grep ' * '", $shellOutput);
        foreach ($shellOutput as $line)
        {
            if (strpos($line, '* ') !== false)
            {

                echo "<div style='clear: both; width: 100%; font-size: 14px; font-family: Helvetica; color: #30121d; background: #bcbf77; padding: 20px; text-align: center;'>Current branch: <span style='color:#fff; font-weight: bold; text-transform: uppercase;'>" .trim(strtolower(str_replace('* ', '', $line))). "</span></div>";

               // echo "Current Branch Name : " . trim(strtolower(str_replace('* ', '', $line)));
            }
        }
        echo "<pre>";
        print_r($shellOutput);
        echo "</pre>";
    }

}
